ipfs_instances = {
    
    ipfs1 = {
        availability_zone = "eu-west-1a"
        instance_type = "t2.medium"
        subnet_id = "private-subnetid-1"
        size = "100"
    }
    ipfs2 = {
        availability_zone = "eu-west-1b"
        instance_type = "t2.medium"
        subnet_id = "private-subnetid-1"
        size = "102"
    }
    ipfs3 = {
        availability_zone = "eu-west-1c"
        instance_type = "t2.medium"
        subnet_id = "private-subnetid-3"
        size = "130"
    }
}