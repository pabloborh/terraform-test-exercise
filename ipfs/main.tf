resource "aws_instance" "ipfs" {
  for_each = var.ipfs_instances

  associate_public_ip_address = false

  ami                         = local.ami
  key_name                    = local.key_name
  vpc_security_group_ids      = [aws_security_group.ipfs.id]

  subnet_id                   = each.value.subnet_id
  availability_zone           = each.value.availability_zone
  instance_type               = each.value.instance_type
}

resource "aws_ebs_volume" "ipfs" {
  for_each = var.ipfs_instances
  
  availability_zone = each.value.availability_zone
  size              = each.value.size
}

resource "aws_volume_attachment" "ipfs_ebs_att" {
  for_each = var.ipfs_instances

  device_name = "/dev/sdh" # Assuming Linux
  volume_id   = aws_ebs_volume.ipfs[each.key].id
  instance_id = aws_instance.ipfs[each.key].id
}

#################################################################
# Importing existing resource 
# > terraform import aws_security_group.ipfs sg-ipfs-arn
# > terraform plan
# > terraform apply
#################################################################
resource "aws_security_group" "ipfs"{
  name        = "ipfs-sg"
  ingress {
    from_port        = 4001
    to_port          = 4001
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"] 
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"] 
    ipv6_cidr_blocks = ["::/0"]
  }
  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"] 
    ipv6_cidr_blocks = ["::/0"]
  }
}