module "ipfs" {
    source = "./ipfs"
    ipfs_instances = var.ipfs_instances
}